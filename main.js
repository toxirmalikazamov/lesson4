// Assignment 4

// 1-task
var str='The Quick Brown Fox' 
function charReverseHandler(str){
    // your code here
    var str2=[]
    for(let i=0; i<str.length; i++){
        if(str[i] == str[i].toUpperCase()){
            str2.push(str[i].toLowerCase())
        }
        else if(str[i] == str[i].toLowerCase()){
            str2.push(str[i].toUpperCase())
        }
    }
    console.log(str2.join(''))
}
charReverseHandler(str)

//2-task
var x = 0;
var array = Array(); 
function createListHandler() {
    // your code here
    array[x] = document.getElementById("text1").value;
    x++;
    document.getElementById("text1").value = "";
    var e = "";
    for (var y = 0; y < array.length; y++) {
        e += "Element " + y + " = " + array[y] + "<br/>";
    }
    document.getElementById("Result").innerHTML = e;
}
    
//3-task
let arr = [1,8,9,2,1,8,3,'a','a']
// your code here
const findDublicateHandler = arr => arr.filter((item,index) => arr.indexOf(item) !== index)
console.log(findDublicateHandler(arr))

// 4-task
let arr=[5,6,7,2,9]
let arr2=[1,2,'c',3,4,'b']
function unionHandler(a, b) {
    // your code here
    var object = {};
    for (var i = a.length-1; i >= 0; -- i)
        object[a[i]] = a[i];
    for (var i = b.length-1; i >= 0; -- i)
        object[b[i]] = b[i];  
    var ret = []      
    for (var i in object) {
        if (object.hasOwnProperty(i))
        ret.push(object[i]);
    }
    return ret;
}
console.log(unionHandler(arr,arr2))

// 5-task
let arr=[NaN,false,0,1,null,"","hello",undefined,true,52,"$"]
function removeFalsy(arr){
    console.log(arr.filter(Boolean))
}
removeFalsy(arr)


//6-task
let str ="WebmAster"
function reverseStringHandler(a,b){
    // your code here
    let ret=0
    a=a.toLowerCase(); b=b.toLowerCase();
    if (a>b)ret=1
    if (b>a)ret=-1
    return ret
}
console.log(str.split('').sort(reverseStringHandler).join(''))